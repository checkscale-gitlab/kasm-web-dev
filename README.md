kasm-web-dev
---

Kasm Web Development Desktop Package


![Alt text](Screenshot.png "Kasm Web Development Desktop")


Build
---

```
$ make build
```

Or root:
```
$ sudo make build
```





Pull package from docker hub;

```
docker pull oytunistrator/kasm-web-dev:dev
```


After add `oytunistrator/kasm-web-dev:dev` to your kasm panel. 

Dependens
---

- [Docker](https://www.docker.com/)
- [Kasm Workspaces](https://www.kasmweb.com/)


Package List:
---
- php 8.x
- composer 2.x
- laravel 8.x
- visual studio code
- sublime
- chromium
- firefox
- discord
- telegram
- filezilla
- signal
- remmina
- microsoft teams (beta)
- libre office
- vlc
- obs
- openshot
- postman
- geany
- gimp
- inkscape

