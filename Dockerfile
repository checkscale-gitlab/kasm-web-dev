FROM kasmweb/core-ubuntu-bionic:1.10.0
USER root

ENV DEBIAN_FRONTEND noninteractive
ENV HOME /home/kasm-default-profile
ENV STARTUPDIR /dockerstartup
ENV INST_SCRIPTS $STARTUPDIR/install
WORKDIR $HOME

######### Customize Container Here ###########

COPY ./scripts/ $INST_SCRIPTS/
COPY ./icon.png /usr/share/extra/icons/icon_default.png
COPY ./wallpaper.jpg /usr/share/extra/backgrounds/bg_default.png
COPY ./config/ $HOME/.config/
RUN tar -xvf $HOME/.config/xfce4.tar.gz -C $HOME/.config/
RUN tar -xvf $HOME/.config/thunar.tar.gz -C $HOME/.config/

RUN bash $INST_SCRIPTS/tools.sh && rm -rf $INST_SCRIPTS/tools.sh
RUN bash $INST_SCRIPTS/java.sh && rm -rf $INST_SCRIPTS/java.sh

RUN bash $INST_SCRIPTS/php.sh && rm -rf $INST_SCRIPTS/php.sh
RUN bash $INST_SCRIPTS/composer.sh && rm -rf $INST_SCRIPTS/composer.sh
RUN bash $INST_SCRIPTS/wp_command.sh && rm -rf $INST_SCRIPTS/wp_command.sh

RUN bash $INST_SCRIPTS/editors.sh && rm -rf $INST_SCRIPTS/editors.sh

RUN bash $INST_SCRIPTS/firefox.sh && rm -rf $INST_SCRIPTS/firefox.sh
RUN bash $INST_SCRIPTS/chrome.sh && rm -rf $INST_SCRIPTS/chrome.sh
RUN bash $INST_SCRIPTS/chromium.sh && rm -rf $INST_SCRIPTS/chromium.sh
RUN bash $INST_SCRIPTS/brave.sh && rm -rf $INST_SCRIPTS/brave.sh
RUN bash $INST_SCRIPTS/edge.sh && rm -rf $INST_SCRIPTS/edge.sh
RUN bash $INST_SCRIPTS/tor_browser.sh && rm -rf $INST_SCRIPTS/tor_browser.sh

RUN bash $INST_SCRIPTS/discord.sh && rm -rf $INST_SCRIPTS/discord.sh
RUN bash $INST_SCRIPTS/teams.sh && rm -rf $INST_SCRIPTS/teams.sh
RUN bash $INST_SCRIPTS/zoom.sh && rm -rf $INST_SCRIPTS/zoom.sh
RUN bash $INST_SCRIPTS/signal.sh && rm -rf $INST_SCRIPTS/signal.sh
RUN bash $INST_SCRIPTS/telegram.sh && rm -rf $INST_SCRIPTS/telegram.sh

RUN bash $INST_SCRIPTS/multimedia.sh && rm -rf $INST_SCRIPTS/multimedia.sh
RUN bash $INST_SCRIPTS/graphics.sh && rm -rf $INST_SCRIPTS/graphics.sh
RUN bash $INST_SCRIPTS/office.sh && rm -rf $INST_SCRIPTS/office.sh

RUN mkdir -p $HOME/Bin && mkdir -p $HOME/Go \
    && echo 'export GOPATH="$HOME/Go"' >> $HOME/.bashrc \
    && echo 'export PATH="$PATH:$HOME/.config/composer/vendor/bin:$HOME/Bin:$GOPATH/bin:$HOME/.symfony/bin"' >> $HOME/.bashrc \
    && echo progress-bar >> $HOME/.curlrc

COPY ./bin/ $HOME/Bin/
RUN chown 1000:1000 $HOME/Bin/*
RUN chmod a+x $HOME/Bin/*

USER 1000
RUN /usr/bin/php /usr/bin/composer global require laravel/installer
RUN wget https://get.symfony.com/cli/installer -O - | bash
USER root

######### End Customizations ###########

RUN chown 1000:0 $HOME
RUN bash $STARTUPDIR/set_user_permission.sh $HOME

ENV HOME /home/kasm-user
WORKDIR $HOME

RUN mkdir -p $HOME && chown -R 1000:0 $HOME

USER 1000
