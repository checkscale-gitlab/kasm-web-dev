#!/bin/bash
set -ex

CHROME_ARGS="--password-store=basic --no-sandbox --ignore-gpu-blocklist --user-data-dir --no-first-run"

DEV_BUILD=$(curl -q https://packages.microsoft.com/repos/edge/pool/main/m/microsoft-edge-dev/ | grep href | grep .deb | sed 's/.*href="//g'  | cut -d '"' -f1 | tail -1)

wget -q -O edge.deb https://packages.microsoft.com/repos/edge/pool/main/m/microsoft-edge-dev/$DEV_BUILD
apt-get install -y ./edge.deb
rm edge.deb

mv /usr/bin/microsoft-edge-dev  /usr/bin/microsoft-edge-dev-orig
cat >/usr/bin/microsoft-edge-dev <<EOL
#!/usr/bin/env bash
sed -i 's/"exited_cleanly":false/"exited_cleanly":true/' ~/.config/microsoft-edge-dev/Default/Preferences
sed -i 's/"exit_type":"Crashed"/"exit_type":"None"/' ~/.config/microsoft-edge-dev/Default/Preferences
/opt/microsoft/msedge-dev/microsoft-edge ${CHROME_ARGS} "\$@"
EOL
chmod +x /usr/bin/microsoft-edge-dev
