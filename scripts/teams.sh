#!/bin/bash
set -ex

curl -L -o teams.deb  "https://go.microsoft.com/fwlink/p/?linkid=2112886&clcid=0x409&culture=en-us&country=us"
apt-get install -y ./teams.deb 
rm teams.deb
