#!/bin/bash
set -ex

curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg
curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add -
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | apt-key add -

echo "deb https://download.sublimetext.com/ apt/stable/" |  tee /etc/apt/sources.list.d/sublime-text.list
echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list
apt-add-repository "deb [arch=$(dpkg --print-architecture)] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
add-apt-repository ppa:vantuz/postman

apt-get install -y code code-insiders sublime-text postman maximus terraform

cp /usr/share/applications/sublime_text.desktop $HOME/Desktop/
cp /usr/share/applications/code.desktop $HOME/Desktop/

chown 1000:1000 $HOME/Desktop/sublime_text.desktop
chown 1000:1000 $HOME/Desktop/code.desktop
