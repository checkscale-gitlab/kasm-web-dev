#!/usr/bin/bash
set -ex

CHROME_ARGS="--password-store=basic --no-sandbox  --ignore-gpu-blocklist --user-data-dir --no-first-run"

apt-get install -y chromium-browser chromium-codecs-ffmpeg

sed -i 's/-stable//g' /usr/share/applications/chromium-browser.desktop

mv /usr/bin/chromium-browser /usr/bin/chromium-browser-orig
cat >/usr/bin/chromium-browser <<EOL
#!/usr/bin/env bash
sed -i 's/"exited_cleanly":false/"exited_cleanly":true/' ~/.config/chromium/Default/Preferences
sed -i 's/"exit_type":"Crashed"/"exit_type":"None"/' ~/.config/chromium/Default/Preferences
/usr/bin/chromium-browser-orig ${CHROME_ARGS} "\$@"
EOL
chmod +x /usr/bin/chromium-browser
cp /usr/bin/chromium-browser /usr/bin/chromium
